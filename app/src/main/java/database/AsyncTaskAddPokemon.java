package database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.pokedexpocket.models.Pokemon;

public class AsyncTaskAddPokemon extends AsyncTask<Pokemon, Void, Long> {

    private PokemonManager db;
    @SuppressLint("StaticFieldLeak")
    private Context c;

    public AsyncTaskAddPokemon(Context context, PokemonManager pm){
        this.db = pm;
        this.c = context;
    }

    @Override
    protected Long doInBackground(Pokemon... pokemons) {
        return this.db.insert(pokemons[0]);
    }

    @Override
    protected void onPostExecute(Long l) {
        if (l != -1){
        Toast.makeText(this.c, "Le pokemon à bien été ajouté", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this.c, "Une erreur c'est produite", Toast.LENGTH_SHORT).show();
        }
    }


}
