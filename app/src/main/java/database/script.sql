CREATE TABLE POKEMON
(
  id int,
  nom VARCHAR(50),
  numpokedex VARCHAR(3),
  type1 VARCHAR(15),
  type2 VARCHAR(15),
  pv int,
  att int,
  def int,
  vit int,
  taille decimal(3,2),
  poids decimal(3,2),
  PRIMARY KEY (id)
);