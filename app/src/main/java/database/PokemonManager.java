package database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.example.pokedexpocket.models.Pokemon;

import java.util.ArrayList;
import java.util.List;

public class PokemonManager{

    private SQLiteDatabase db;

    private DatabaseManager dbHelper;

    // Constructeur
    public PokemonManager(Context context){
        dbHelper = new DatabaseManager(context);
        open();
    }

    private void open() throws SQLiteException {
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            db = dbHelper.getReadableDatabase();
        }
    }

    public void close() {
        db.close();
    }


    private ContentValues pokemonToContentValues(Pokemon pok){
        ContentValues values = new ContentValues();
        values.put("id", pok.getId());
        values.put("nom", pok.getId());
        values.put("numPokedex", pok.getId());
        values.put("type1", pok.getId());
        values.put("type2", pok.getId());
        values.put("pv", pok.getId());
        values.put("att", pok.getId());
        values.put("def", pok.getId());
        values.put("vit", pok.getId());
        values.put("taille", pok.getId());
        values.put("poids", pok.getId());
        return values;
    }

    private Pokemon cursorToPokemon(Cursor c){
        Pokemon p = null;
        if (c.moveToFirst()) {
            p = new Pokemon(
                    c.getInt(c.getColumnIndex("id")),
                    c.getString(c.getColumnIndex("nom")),
                    c.getString(c.getColumnIndex("numpokedex")),
                    c.getInt(c.getColumnIndex("pv")),
                    c.getInt(c.getColumnIndex("att")),
                    c.getInt(c.getColumnIndex("def")),
                    c.getInt(c.getColumnIndex("vit")),
                    c.getDouble(c.getColumnIndex("taille")),
                    c.getDouble(c.getColumnIndex("poids")),
                    c.getString(c.getColumnIndex("type1")),
                    c.getString(c.getColumnIndex("type2"))
            );
            c.close();
        }
        return p;
    }

    public long insert(Pokemon pok) {
        // Ajout d'un enregistrement dans la table

        ContentValues values = pokemonToContentValues(pok);

        // insert() retourne l'id du nouvel enregistrement inséré, ou -1 en cas d'erreur
        return db.insert(DatabaseManager.Constants.MY_TABLE,null,values);
    }

    public int update(Pokemon pok) {
        // modification d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la requête

        ContentValues values = pokemonToContentValues(pok);

        String where = "id = ?";
        String[] whereArgs = {pok.getId()+""};

        return db.update(DatabaseManager.Constants.MY_TABLE, values, where, whereArgs);
    }

    public int delete(Pokemon pok) {
        // suppression d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la clause WHERE, 0 sinon

        String where = "id = ?";
        String[] whereArgs = {pok.getId()+""};

        return db.delete(DatabaseManager.Constants.MY_TABLE, where, whereArgs);
    }

    public Pokemon get(int id) {
        // Retourne l'pok dont l'id est passé en paramètre
        Pokemon p = null;
        Cursor c = db.rawQuery("SELECT * FROM "+ DatabaseManager.Constants.MY_TABLE+" WHERE id ="+id, null);
        if (c.moveToFirst()) {
            p = new Pokemon(
                    c.getInt(c.getColumnIndex("id")),
                    c.getString(c.getColumnIndex("nom")),
                    c.getString(c.getColumnIndex("numpokedex")),
                    c.getInt(c.getColumnIndex("pv")),
                    c.getInt(c.getColumnIndex("att")),
                    c.getInt(c.getColumnIndex("def")),
                    c.getInt(c.getColumnIndex("vit")),
                    c.getDouble(c.getColumnIndex("taille")),
                    c.getDouble(c.getColumnIndex("poids")),
                    c.getString(c.getColumnIndex("type1")),
                    c.getString(c.getColumnIndex("type2"))
            );
            c.close();
        }

        return p;
    }

    public List<Pokemon> getAll() {
        // sélection de tous les enregistrements de la table
        List<Pokemon> res = new ArrayList<>();

        @SuppressLint("Recycle") Cursor c = db.rawQuery("SELECT * FROM "+ DatabaseManager.Constants.MY_TABLE+";", null);
        while (c.moveToNext()){
            res.add(cursorToPokemon(c));
        }
        c.close();

        return res;
    }

}