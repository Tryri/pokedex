package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class DatabaseManager extends SQLiteOpenHelper {
       /**
     * @goals This class aims to show the constant to use for the DBOpenHelper
     */
    public static class Constants implements BaseColumns {
        // The database name
        public static final String DATABASE_NAME = "pokemon.db";
        // The database version
        public static final int DATABASE_VERSION = 1;
        // The table Name
        public static final String MY_TABLE = "pokemon";

        // ## Column name ##
        // My Column ID and the associated explanation for end-users
        public static final String ID_POKEMON = "id";
        public static final String NAME_POKEMON = "nom";
        public static final String NUM_POKEMON = "numpokedex";
        public static final String TYPE1_POKEMON = "type1";
        public static final String TYPE2_POKEMON = "type2";
        public static final String PV_POKEMON = "pv";
        public static final String ATT_POKEMON = "att";
        public static final String DEF_POKEMON = "def";
        public static final String VIT_POKEMON = "vit";
        public static final String TAILLE_POKEMON = "taille";
        public static final String POIDS_POKEMON = "poids";

        // Indexes des colonnes
        // The index of the column ID
        public static final int ID_COLUMN = 1;
        public static final int NAME_COLUMN = 2;
        public static final int NUM_COLUMN = 3;
        public static final int TYPE1_COLUMN = 4;
        public static final int TYPE2_COLUMN = 5;
        public static final int PV_COLUMN = 6;
        public static final int ATT_COLUMN = 7;
        public static final int DEF_COLUMN = 8;
        public static final int VIT_COLUMN = 9;
        public static final int TAILLE_COLUMN = 10;
        public static final int POIDS_COLUMN = 11;

    }

    /**
     * The static string to create the database.
     */
    private static final String DATABASE_CREATE = "create table "+ Constants.MY_TABLE + "("
            + Constants.ID_POKEMON+ " int primary key, "
            + Constants.NAME_POKEMON+ " varchar(50), "
            + Constants.NUM_POKEMON + " varchar(3), "
            + Constants.TYPE1_POKEMON + " varchar(15), "
            + Constants.TYPE2_POKEMON + " varchar(15), "
            + Constants.PV_POKEMON + " int, "
            + Constants.ATT_POKEMON + " int, "
            + Constants.DEF_POKEMON + " int, "
            + Constants.VIT_POKEMON + " int, "
            + Constants.TAILLE_POKEMON + " decimal(3,2), "
            + Constants.POIDS_POKEMON + " decimal(3,2)); "
            ;

    public DatabaseManager(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("DBOpenHelper", "Mise à jour de la version " + oldVersion
                + " vers la version " + newVersion
                + ", les anciennes données seront détruites ");
        // Drop the old database
        db.execSQL("DROP TABLE IF EXISTS " + Constants.MY_TABLE);
        // Create the new one
        onCreate(db);
        // or do a smartest stuff
    }
}