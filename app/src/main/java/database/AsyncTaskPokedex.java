package database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.pokedexpocket.models.Pokemon;

import java.util.ArrayList;
import java.util.List;

public class AsyncTaskPokedex extends AsyncTask<Boolean, Void, Boolean> {

    private PokemonManager db;
    @SuppressLint("StaticFieldLeak")
    private Context c;
    private List<Pokemon> listeData;

    public AsyncTaskPokedex(Context context, PokemonManager pm){
        this.db = pm;
        this.c = context;
        listeData = new ArrayList<>();
    }


    @Override
    protected Boolean doInBackground(Boolean... voids) {
        this.listeData = this.db.getAll();
        return true;
    }

    public List<Pokemon> getListeData(){
        return this.listeData;
    }
}
