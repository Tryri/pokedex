package com.example.pokedexpocket;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class AdapterType extends RecyclerView.Adapter<AdapterType.MyViewHolder> {
    
    private List<String> listType;
    
    public AdapterType(List<String> list){
        this.listType = list;
    }
    
    @Override
    public int getItemCount() {
        return listType.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cell_type, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String type = listType.get(position);
        holder.display(type);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private final TextView name;

        public MyViewHolder(final View itemView){
            super(itemView);
            this.name = itemView.findViewById(R.id.text_type);

        }

        public void display(String type){
            this.name.setText(type);
        }

    }

}
