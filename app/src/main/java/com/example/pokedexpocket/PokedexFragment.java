package com.example.pokedexpocket;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pokedexpocket.models.Pokemon;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Arrays;
import java.util.List;

import database.AsyncTaskPokedex;
import database.PokemonManager;

public class PokedexFragment extends Fragment {

    private List<Pokemon> data = Arrays.asList( new Pokemon("Bulbizarre", 45, 49, 49, 45, 0.7, 6.9, "Plante", "Poison"),
                new Pokemon("Salameche", 39, 52, 43, 65, 0.6, 8.5, "Feu", ""),
                new Pokemon("Carapuce", 44, 48, 65, 43, 0.5, 9.0, "Eau", ""),
                new Pokemon("Pikachu", 35, 55, 40, 90, 0.4, 6.0, "Electrik", ""));;

    private AdapterPokemon adapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_pokedex, container, false);

        PokemonManager pm = new PokemonManager(getContext());
        FloatingActionButton btn = root.findViewById(R.id.addPok);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AjoutPokemon.class));
            }
        });

        //Récupération de la liste des données de la BD
        //Décommentez les 3 lignes suivantes et commentez la ligne pour voir l'erreur de bd
        /*
        AsyncTaskPokedex atp = new AsyncTaskPokedex(getContext(), pm);
        atp.execute(true);
        List<Pokemon> data = atp.getListeData();
         */

        if (data.isEmpty()){
            Toast.makeText(getContext(),"BD vide" ,Toast.LENGTH_SHORT).show();
        }
        else{
            RecyclerView rv = root.findViewById(R.id.list_pokemon);
            adapter = new AdapterPokemon(data);
            rv.setLayoutManager(new LinearLayoutManager(root.getContext()));
            rv.setAdapter(adapter);
        }
        return root;
    }

}
