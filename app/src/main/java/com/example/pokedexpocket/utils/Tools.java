package com.example.pokedexpocket.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Tools {

    private static int id = 0;

    private Map<String, ArrayList<String>> relationType;

    public Tools(){
        this.relationType = new HashMap<>();

        ArrayList<String> plante = new ArrayList<>();
        plante.add("Eau");
        plante.add("Sol");
        plante.add("Roche");
        ArrayList<String> vol = new ArrayList<>();
        vol.add("Plante");
        vol.add("Combat");
        vol.add("Insecte");
        ArrayList<String> spectre = new ArrayList<>();
        spectre.add("Psy");
        spectre.add("Spectre");
        ArrayList<String> feu = new ArrayList<>();
        feu.add("Plante");
        feu.add("Glace");
        feu.add("Insecte");
        feu.add("Acier");
        ArrayList<String> tenebres = new ArrayList<>();
        tenebres.add("Psy");
        tenebres.add("Spectre");
        ArrayList<String> eau = new ArrayList<>();
        eau.add("Feu");
        eau.add("Sol");
        eau.add("Roche");
        ArrayList<String> fee = new ArrayList<>();
        fee.add("Combat");
        fee.add("Dragon");
        fee.add("Ténèbres");
        ArrayList<String> normal = new ArrayList<>();
        ArrayList<String> insecte = new ArrayList<>();
        insecte.add("Plante");
        insecte.add("Psy");
        insecte.add("Ténèbres");
        ArrayList<String> electrik = new ArrayList<>();
        electrik.add("Eau");
        electrik.add("Vol");
        ArrayList<String> combat = new ArrayList<>();
        combat.add("Normal");
        combat.add("Glace");
        combat.add("Ténèbres");
        combat.add("Acier");
        ArrayList<String> glace = new ArrayList<>();
        glace.add("Plante");
        glace.add("Sol");
        glace.add("Vol");
        glace.add("Dragon");
        ArrayList<String> roche = new ArrayList<>();
        roche.add("Fée");
        roche.add("Glace");
        roche.add("Vol");
        roche.add("Insecte");
        ArrayList<String> poison = new ArrayList<>();
        poison.add("Plante");
        poison.add("Fée");
        ArrayList<String> sol = new ArrayList<>();
        sol.add("Feu");
        sol.add("Electrik");
        sol.add("Poison");
        sol.add("Roche");
        sol.add("Acier");
        ArrayList<String> psy = new ArrayList<>();
        psy.add("Combat");
        psy.add("Poison");
        ArrayList<String> dragon = new ArrayList<>();
        dragon.add("Dragon");
        ArrayList<String> acier = new ArrayList<>();
        acier.add("Glace");
        acier.add("Roche");
        acier.add("Fée");
        this.relationType.put("Plante", plante);
        this.relationType.put("Vol", vol);
        this.relationType.put("Spectre", spectre);
        this.relationType.put("Feu", feu);
        this.relationType.put("Ténèbres", tenebres);
        this.relationType.put("Eau", eau);
        this.relationType.put("Fée", fee);
        this.relationType.put("Normal", normal);
        this.relationType.put("Insecte", insecte);
        this.relationType.put("Electrik", electrik);
        this.relationType.put("Combat", combat);
        this.relationType.put("Glace", glace);
        this.relationType.put("Roche", roche);
        this.relationType.put("Poison", poison);
        this.relationType.put("Sol", sol);
        this.relationType.put("Psy", psy);
        this.relationType.put("Dragon", dragon);
        this.relationType.put("Acier", acier);

    }

    public static int getStartId(){
        id ++;
        return id;
    }

    public static void setId(int id) {
        Tools.id = id;
    }



    public ArrayList<String> getFort(String type){
        return this.relationType.get(type);
    }


    public ArrayList<String> getFaible(String type){
        ArrayList<String> res = new ArrayList<>();
        for (String s : this.relationType.keySet()){
            if (Objects.requireNonNull(this.relationType.get(s)).contains(type)){
                res.add(s);
            }
        }
        return res;
    }
}

