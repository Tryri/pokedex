package com.example.pokedexpocket;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pokedexpocket.utils.Tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class CalculatorFragment extends Fragment {

    private RecyclerView rv;
    private AdapterType adapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_calculator, container, false);
        rv = root.findViewById(R.id.listTypeCalcule);
        rv.setLayoutManager(new LinearLayoutManager(root.getContext()));

        final Spinner spinnerType = root.findViewById(R.id.spinnerType);

        ArrayAdapter<CharSequence> adapterSpinner = ArrayAdapter.createFromResource(Objects.requireNonNull(this.getContext()), R.array.type_array, android.R.layout.simple_spinner_item);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adapterSpinner);

        RadioGroup radioGroup = root.findViewById(R.id.radioGroup);
        final RadioButton radioButton1 = root.findViewById(R.id.radioButton);
        final RadioButton radioButton2 = root.findViewById(R.id.radioButton2);
        Button research = root.findViewById(R.id.researchType);

        research.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!spinnerType.getSelectedItem().toString().equals("Aucun")) {
                    Tools t = new Tools();
                    ArrayList<String> listeSearch = null;
                    if (radioButton1.isChecked()) {
                        listeSearch = t.getFort(spinnerType.getSelectedItem().toString());
                    } else if (radioButton2.isChecked()) {
                        listeSearch = t.getFaible(spinnerType.getSelectedItem().toString());
                    }
                    adapter = new AdapterType(listeSearch);
                    rv.setAdapter(adapter);
                }
                else{
                    Toast.makeText(getContext(), "Choissisez un type valide", Toast.LENGTH_LONG).show();
                }
            }
        });

        radioGroup.check(0);

        return root;
    }
}
