package com.example.pokedexpocket;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.pokedexpocket.models.Pokemon;

import database.AsyncTaskAddPokemon;
import database.DatabaseManager;
import database.PokemonManager;

public class AjoutPokemon extends AppCompatActivity {

    private EditText edit_name, edit_att, edit_def, edit_vit, edit_taille, edit_poids, edit_pv;
    private Spinner spinnerType1, spinnerType2;

    private PokemonManager pm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_pokemon);
        ArrayAdapter<CharSequence> adapterSpinner = ArrayAdapter.createFromResource(getApplicationContext(), R.array.type_array, android.R.layout.simple_spinner_item);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        this.spinnerType1 = findViewById(R.id.spinner_type1);
        this.spinnerType2 = findViewById(R.id.spinner_type2);
        spinnerType1.setAdapter(adapterSpinner);
        spinnerType2.setAdapter(adapterSpinner);

        this.edit_pv = findViewById(R.id.edit_pokemon_pv);
        this.edit_name = findViewById(R.id.edit_pokemon_name);
        this.edit_att = findViewById(R.id.edit_pokemon_att);
        this.edit_def = findViewById(R.id.edit_pokemon_def);
        this.edit_vit = findViewById(R.id.edit_pokemon_vit);
        this.edit_taille = findViewById(R.id.edit_pokemon_taille);
        this.edit_poids = findViewById(R.id.edit_pokemon_poids);

        Button btn = findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_pv.getText().toString().equals("") ||
                    edit_att.getText().toString().equals("") ||
                    edit_def.getText().toString().equals("") ||
                    edit_vit.getText().toString().equals("") ||
                    edit_name.getText().toString().equals("") ||
                    edit_taille.getText().toString().equals("") ||
                    edit_poids.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Tout les champs doivent être rempli", Toast.LENGTH_LONG).show();
                }else if(spinnerType1.getSelectedItem().toString().equals("Aucun")){
                    Toast.makeText(getApplicationContext(), "Le pokemon doit avoir son premier type valide", Toast.LENGTH_LONG).show();
                }else{
                    Pokemon p = new Pokemon(edit_name.getText().toString(),
                                            Integer.parseInt(edit_pv.getText().toString()),
                                            Integer.parseInt(edit_att.getText().toString()),
                                            Integer.parseInt(edit_def.getText().toString()),
                                            Integer.parseInt(edit_vit.getText().toString()),
                                            Double.parseDouble(edit_taille.getText().toString()),
                                            Double.parseDouble(edit_poids.getText().toString()),
                                            spinnerType1.getSelectedItem().toString(),
                                            spinnerType2.getSelectedItem().toString().equals("Aucun") ? "" : spinnerType2.getSelectedItem().toString()
                                            );

                    //Appel d'une AsyncTask pour mettre à jour la BD
                    /*
                    AsyncTaskAddPokemon atap = new AsyncTaskAddPokemon(getApplicationContext(), pm);
                    atap.execute(p);
                    */
                    cleanForm();

                }
            }
        });
    }

    private void cleanForm(){
            this.edit_pv.setText("");
            this.edit_name.setText("");
            this.edit_att.setText("");
            this.edit_def.setText("");
            this.edit_vit.setText("");
            this.edit_taille.setText("");
            this.edit_poids.setText("");

        }

    @Override
    protected void onResume() {
        super.onResume();
        this.pm = new PokemonManager(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.pm.close();
    }
}
