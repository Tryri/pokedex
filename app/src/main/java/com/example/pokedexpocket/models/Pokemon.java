package com.example.pokedexpocket.models;

import android.util.Pair;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.example.pokedexpocket.utils.Tools;

import java.util.Objects;

@Entity
public class Pokemon {

    @PrimaryKey
    private int id;
    private int pv, att, def, vit;
    private double taille, poids;
    private String nom, numPokedex;
//    private ImageView img;
    private String type1, type2;

    public Pokemon(String nom, String numPokedex){
        this.id = Tools.getStartId();
        this.nom = nom;
        this.numPokedex = numPokedex;
    }

    public Pokemon(String nom, String numPokedex, int pv, int att, int def, int vit, double taille, double poids, String t1, String t2){
        this.id = Tools.getStartId();
        this.nom = nom;
        this.numPokedex = numPokedex;
        this.pv = pv;
        this.att = att;
        this.def = def;
        this.vit = vit;
        this.taille = taille;
        this.poids = poids;
        this.type1 = t1;
        this.type2 = t2;
    }

    public Pokemon(String nom, int pv, int att, int def, int vit, double taille, double poids, String t1, String t2){
        this.id = Tools.getStartId();
        this.nom = nom;
        if (this.id < 10){
            this.numPokedex = "00"+this.id;
        }else if(this.id < 100){
            this.numPokedex = "0"+this.id;
        }else{
            this.numPokedex = Integer.toString(this.id);
        }
        this.pv = pv;
        this.att = att;
        this.def = def;
        this.vit = vit;
        this.taille = taille;
        this.poids = poids;
        this.type1 = t1;
        this.type2 = t2;
    }

    public Pokemon(int id, String nom, String numPokedex, int pv, int att, int def, int vit, double taille, double poids, String t1, String t2){
        this.id = id;
        this.nom = nom;
        this.numPokedex = numPokedex;
        this.pv = pv;
        this.att = att;
        this.def = def;
        this.vit = vit;
        this.taille = taille;
        this.poids = poids;
        this.type1 = t1;
        this.type2 = t2;
    }

    public int getAtt() {
        return att;
    }

    public int getDef() {
        return def;
    }

    public String getNumPokedex() {
        return numPokedex;
    }

    public int getId() {
        return id;
    }

    public double getPoids() {
        return poids;
    }

    public int getPv() {
        return pv;
    }

    public double getTaille() {
        return taille;
    }

    public int getVit() {
        return vit;
    }

    public String getNom() {
        return nom;
    }

    public String getType1() {
        return type1;
    }

    public String getType2() {
        return type2;
    }

    public Pair<String, String> getType(){
        return new Pair<>(this.type1, this.type2);}

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setNumPokedex(String numPokedex) {
        this.numPokedex = numPokedex;
    }

    public void setAtt(int att) {
        this.att = att;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public void setPoids(double poids) {
        this.poids = poids;
    }

    public void setPv(int pv) {
        this.pv = pv;
    }

    public void setTaille(double taille) {
        this.taille = taille;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public void setType2(String type2) {
        this.type2 = type2;
    }

    public void setVit(int vit) {
        this.vit = vit;
    }

    @NonNull
    @Override
    public String toString() {
        return this.numPokedex+": "+this.nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pokemon pokemon = (Pokemon) o;
        return numPokedex == pokemon.numPokedex &&
                Objects.equals(nom, pokemon.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numPokedex, nom);
    }
}
