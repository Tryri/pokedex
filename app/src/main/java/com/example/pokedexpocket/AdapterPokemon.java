package com.example.pokedexpocket;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pokedexpocket.models.Pokemon;

import java.util.List;


public class AdapterPokemon extends RecyclerView.Adapter<AdapterPokemon.MyViewHolder> {

    private List<Pokemon> characters;

    public AdapterPokemon(List<Pokemon> c){
        this.characters = c;
    }

    public void setCharacters(List<Pokemon> characters) {
        this.characters = characters;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return characters.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_cell, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Pokemon pok = characters.get(position);
        holder.display(pok);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private final TextView name;
        private final TextView type1;
        private final TextView type2;
        private Pokemon pokemon;

        public MyViewHolder(final View itemView){
            super(itemView);

            this.name = itemView.findViewById(R.id.name);
            this.type1 = itemView.findViewById(R.id.type);
            this.type2 = itemView.findViewById(R.id.type2);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle(pokemon.getNom())
                            .setMessage(
                                        "Numéro Pokedex: "+pokemon.getNumPokedex()+"\n"+
                                        "PV: "+pokemon.getPv()+"\n"+
                                        "Att: "+pokemon.getAtt()+"\n"+
                                        "Def: "+pokemon.getDef()+"\n"+
                                        "Vit: "+pokemon.getVit()+"\n"+
                                        "Taille: "+pokemon.getTaille()+"\n"+
                                        "Poids: "+pokemon.getPoids())
                            .show();
                }
            });

        }

        public void display(Pokemon pok){
            this.pokemon=pok;
            this.name.setText(pok.getNom());
            this.type1.setText(pok.getType1());
            this.type2.setText(pok.getType2());
        }

    }

}
