Notre application est composée de deux fonctionnalités:

1. Un calculateur donnant donnant une liste de type selon plusieurs critères
2. Un pokedex dans lequel nous pouvons ajouter des pokemon en indiquant leurs informations (cliquer sur les pokemon fait apparaitre leur détails)

Suite à certain problème de base de données la fonctionnalité d'ajout de pokemon au pokedex n'est pas fonctionnel, 
pour cette démo nous avons mis en place une liste de 4 pokemons afin de vous fournir un visuel de notre travail


Evolutions possibles:

- mis en place d'une bd fonctionnel
- ajout de pokemon
- pris en charge du double type dans le calculateur
- ajout du mini-jeu de la série "Who that ?"
- appel à la galerie ou autre forme de stockage afin d'attribuer une image à un pokemon
